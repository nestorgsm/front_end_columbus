import { createStore } from "vuex";

import auth from "../modules/auth/store";
import client from "../modules/client/store";

const store = createStore({
  modules: {
    auth,
    client,
  },
});

export default store;
