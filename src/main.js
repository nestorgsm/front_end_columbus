import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import PrimeVue from "primevue/config";
import VueParticles from "vue-particles";

import "primevue/resources/themes/saga-blue/theme.css";
import "primevue/resources/primevue.min.css";
import "primeicons/primeicons.css";
import "primeflex/primeflex.min.css";
import "primeflex/primeflex.css";

import InputText from "primevue/inputtext";
import Button from "primevue/button";
import InputMask from "primevue/inputmask";
import Toolbar from "primevue/toolbar";
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import InputNumber from "primevue/inputnumber";
import BadgeDirective from "primevue/badgedirective";

createApp(App)
  .use(store)
  .use(router)
  .use(PrimeVue, {
    locale: {
      weak: "Débil",
      medium: "Media",
      strong: "Fuerte",
    },
  })
  .use(VueParticles)
  .component("InputText", InputText)
  .component("Button", Button)
  .component("InputMask", InputMask)
  .component("Toolbar", Toolbar)
  .component("DataTable", DataTable)
  .component("Column", Column)
  .component("InputNumber", InputNumber)
  .directive("badge", BadgeDirective)
  .mount("#app");
