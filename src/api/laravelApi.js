import axios from "axios";

const journalApi = axios.create({
  baseURL: "http://127.0.0.1:8000/api",
});

journalApi.interceptors.request.use((config) => {
  config.headers = {
    authorization: `Bearer ${localStorage.getItem("access_token")}`,
  };
  return config;
});

export default journalApi;
