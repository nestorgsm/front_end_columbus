import { createRouter, createWebHashHistory } from "vue-router";
import NoPageFound from "@/views/NoPageFoundView.vue";
import isAuthenticatedGuard from "@/modules/auth/router/auth-guard";

import LoginComponent from "../modules/auth/components/LoginComponent.vue";
import RegisterComponent from "../modules/auth/components/RegisterComponent.vue";
import clientRouter from "../modules/client/router";

const routes = [
  {
    path: "/",
    name: "login",
    component: LoginComponent,
  },
  {
    path: "/registrarse",
    name: "registrarse",
    component: RegisterComponent,
  },
  {
    path: "/client",
    beforeEnter: [isAuthenticatedGuard],
    ...clientRouter,
  },
  {
    path: "/:pathMatch(.*)*",
    component: NoPageFound,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
