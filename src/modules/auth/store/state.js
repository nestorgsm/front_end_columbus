export default () => ({
  status: "authenticating", // 'authenticated', 'not-authenticated', 'authenticating'
  user: null,
  access_token: null,
});
