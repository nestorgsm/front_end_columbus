// export const myAction = async ({commit}) => {

// }

import api from "@/api/laravelApi";

export const registerNewUser = async (_, newUser) => {
  try {
    const { data: info } = await api.post(`/register`, {
      ...newUser,
    });
    const { status, message } = info;
    if (status != "success") return { ok: false, message };

    return { ok: true, message: "Usuario creado con exito" };
  } catch (err) {
    let msg;
    if (err.response) {
      msg = err.response.data.message;
    } else if (err.request) {
      msg = err.request;
    } else {
      msg = err.message;
    }
    return { ok: false, message: msg };
  }
};

export const signInUser = async ({ commit }, usuario) => {
  try {
    const { email, password } = usuario;

    const { data } = await api.post("/login", {
      email,
      password,
    });
    const { status, message } = data;
    if (status == "error") return { ok: false, message: message };

    const { user, access_token } = data;

    commit("loginUser", { user, access_token });

    return { ok: true, message: "ok", tipo_user: user.tipo_user };
  } catch (err) {
    let msg;
    if (err.response) {
      msg = err.response.data.message;
    } else if (err.request) {
      msg = err.request;
    }

    if (!msg) {
      msg = err.message;
    }
    return { ok: false, message: msg };
  }
};

export const verifyAuth = async ({ commit }) => {
  const access_token = localStorage.getItem("access_token");
  if (!access_token) {
    commit("logout");
    return { ok: false, message: "No hay token" };
  }

  try {
    const { data } = await api.post("/getInfoByToken", {
      access_token,
    });
    const { status, user, message } = data;

    if (status != "success") {
      commit("logout");
      return { ok: false, message };
    }

    commit("loginUser", { user, access_token });
    return { ok: true, message };
  } catch (err) {
    let msg;
    if (err.response) {
      msg = err.response.data.message;
    } else if (err.request) {
      msg = err.request;
    }
    if (!msg) {
      msg = err.message;
    }
    return { ok: false, message: msg };
  }
};
