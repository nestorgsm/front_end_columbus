import { ref } from "vue";

import useVuelidate from "@vuelidate/core";
import { required, email } from "@vuelidate/validators";

import { useRouter } from "vue-router";
import Swal from "sweetalert2";

import { useStore } from "vuex";

const useRegister = () => {
  const store = useStore();
  const submitted = ref(false);
  const router = useRouter();
  const error = ref(false);

  const data = ref({
    email: "",
    password: "",
  });

  const rules = {
    email: { required, email },
    password: { required },
  };

  const v$ = useVuelidate(rules, data);

  const login = async (isFormValid) => {
    error.value = false;
    submitted.value = true;

    if (!isFormValid) {
      submitted.value = false;
      error.value = true;
      return;
    }

    const { ok, message } = await store.dispatch("auth/signInUser", data.value);

    if (!ok) Swal.fire("Error", message, "error");
    else router.push({ name: "client-home" });

    submitted.value = false;
  };

  return {
    v$,
    submitted,
    login,
    error,
  };
};

export default useRegister;
