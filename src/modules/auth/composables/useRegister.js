import { ref } from "vue";

import useVuelidate from "@vuelidate/core";
import { required, email } from "@vuelidate/validators";

import { useRouter } from "vue-router";
import Swal from "sweetalert2";

import { useStore } from "vuex";

const useRegister = () => {
  const store = useStore();
  const items = ref([{}]);
  const submitted = ref(false);
  const options = ref(["Hombre", "Mujer"]);
  const router = useRouter();
  const error = ref(false);

  const data = ref({
    name: "",
    age: "",
    gender: "Hombre",
    email: "",
    password: "",
  });

  const rules = {
    name: { required },
    age: { required },
    gender: { required },
    email: { required, email },
    password: { required },
  };

  const v$ = useVuelidate(rules, data);

  const registerNewUser = async (isFormValid) => {
    error.value = false;
    submitted.value = true;

    if (!isFormValid) {
      submitted.value = false;
      error.value = true;
      return;
    }
    const { ok, message } = await store.dispatch(
      "auth/registerNewUser",
      data.value
    );
    if (!ok) Swal.fire("Error", message, "error");
    else {
      const { ok: okk, message: msg } = await store.dispatch(
        "auth/signInUser",
        data.value
      );

      if (!okk) Swal.fire("Error", msg, "error");
      else router.push({ name: "client-home" });
    }

    submitted.value = false;
  };

  return {
    items,
    options,
    v$,
    submitted,
    registerNewUser,
    error,
  };
};

export default useRegister;
