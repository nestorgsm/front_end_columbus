import { ref } from "vue";

import Swal from "sweetalert2";

import { useStore } from "vuex";

const useHome = () => {
  const store = useStore();
  const products = ref([]);
  const layout = ref("grid");
  const numDifferentProducts = ref([]);

  const getProducts = async () => {
    const resp = await store.dispatch("client/getProducts");
    return resp;
  };

  const addDifferentProduct = async (product_id) => {
    if (!numDifferentProducts.value[product_id])
      numDifferentProducts.value[product_id] = 0;

    numDifferentProducts.value[product_id] += 1;
    await store.dispatch("client/setADifferentProduct", {
      product: product_id,
    });
  };

  return {
    getProducts,
    products,
    Swal,
    layout,
    addDifferentProduct,
    numDifferentProducts,
  };
};

export default useHome;
