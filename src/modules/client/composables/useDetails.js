import { ref } from "vue";

import Swal from "sweetalert2";
import { FilterMatchMode, FilterOperator } from "primevue/api";

import { useStore } from "vuex";

import scstore from "@/store";

import { useRouter } from "vue-router";

const useHome = () => {
  const store = useStore();
  const products = ref([]);
  const router = useRouter();
  const loading = ref(false);

  const columns = ref([
    { field: "id", header: "ID" },
    { field: "name", header: "Producto" },
    { field: "description", header: "Descripción" },
    { field: "price", header: "Precio" },
    { field: "path_img", header: "Imagen" },
    { field: "stock", header: "Stock" },
  ]);

  const filters = ref({
    global: { value: null, matchMode: FilterMatchMode.CONTAINS },
    name: {
      operator: FilterOperator.AND,
      constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
    },
    description: {
      operator: FilterOperator.AND,
      constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
    },
    price: {
      operator: FilterOperator.AND,
      constraints: [{ value: null, matchMode: FilterMatchMode.STARTS_WITH }],
    },
  });

  const getProductsByIds = async (dfProducts) => {
    const resp = await store.dispatch("client/getProductsByIds", dfProducts);
    return resp;
  };

  const addProduct = (product_id) => {
    store.dispatch("client/addProduct", {
      product: product_id,
    });
  };

  const removeProduct = (product_id) => {
    store.dispatch("client/subProduct", {
      product: product_id,
    });
  };

  const buyProducts = async () => {
    loading.value = true;
    const { ok, message } = await store.dispatch("client/buyProducts", {
      products: scstore.state.client.numProducts,
    });
    loading.value = false;
    if (!ok) return Swal.fire("Error", message, "error");
    Swal.fire(
      "Exito",
      "Compra registrada correctamente, se actualizó nuestro stock!",
      "success"
    );
    router.push({ name: "client-home" });
  };

  return {
    getProductsByIds,
    products,
    columns,
    Swal,
    filters,
    addProduct,
    removeProduct,
    scstore,
    buyProducts,
    loading,
  };
};

export default useHome;
