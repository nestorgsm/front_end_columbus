import isAuthenticatedGuard from "@/modules/auth/router/auth-guard";
export default {
  beforeEnter: [isAuthenticatedGuard],
  component: () => import("@/layout/MainLayout.vue"),
  // props: () => {
  //   return {
  //     menuItems: [
  //       {
  //         label: "Dashboard",
  //         icon: "fa-solid fa-chart-column",
  //         to: "/admin",
  //       },
  //       {
  //         label: "Accesos",
  //         icon: "fa-solid fa-user-pen",
  //         to: "/admin/accesos",
  //       },
  //       {
  //         label: "Ventanas",
  //         icon: "fa-solid fa-clock",
  //         to: "/admin/ventana",
  //       },
  //     ],
  //   };
  // },
  children: [
    {
      path: "",
      name: "client-home",
      beforeEnter: [isAuthenticatedGuard],
      component: () => import("@/modules/client/views/HomeView.vue"),
    },
    {
      path: "details",
      name: "client-details",
      beforeEnter: [isAuthenticatedGuard],
      component: () => import("@/modules/client/views/DetailsView.vue"),
    },
  ],
};
