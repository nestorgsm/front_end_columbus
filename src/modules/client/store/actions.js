// export const myAction = async ({commit}) => {

// }

import api from "@/api/laravelApi";

export const getProductsByIds = async ({ commit }, dfProducts) => {
  try {
    const { data } = await api.post("/client/getProductsByIds", {
      ids: dfProducts ? dfProducts : 0,
    });
    const { status, message, product } = data;
    if (status != "success") return { ok: false, message };

    dfProducts.forEach(function (element) {
      let obj = { id: element, cantidad: 1 };
      commit("setNumOfEachProduct", obj);
    });

    return { ok: true, message, product };
  } catch (err) {
    let msg;
    if (err.response) {
      msg = err.response.data.message;
    } else if (err.request) {
      msg = err.request;
    } else {
      msg = err.message;
    }
    return { ok: false, message: msg };
  }
};

export const getProducts = async () => {
  try {
    const { data } = await api.post("/client/getProducts");
    const { status, message, product } = data;
    if (status != "success") return { ok: false, message };

    return { ok: true, message, product };
  } catch (err) {
    let msg;
    if (err.response) {
      msg = err.response.data.message;
    } else if (err.request) {
      msg = err.request;
    } else {
      msg = err.message;
    }
    return { ok: false, message: msg };
  }
};

export const setADifferentProduct = ({ commit }, { product }) => {
  commit("setADifferentProduct", product);
};

export const addProduct = ({ commit }, { product }) => {
  commit("addProduct", product);
};

export const subProduct = ({ commit }, { product }) => {
  commit("subProduct", product);
};

export const buyProducts = async ({ commit }, { products }) => {
  try {
    const { data } = await api.post("/client/buyProducts", {
      products: [...products],
    });
    const { status, message } = data;
    if (status != "success") return { ok: false, message };

    commit("onLogOut");
    return { ok: true, message };
  } catch (err) {
    let msg;
    if (err.response) {
      msg = err.response.data.message;
    } else if (err.request) {
      msg = err.request;
    } else {
      msg = err.message;
    }
    return { ok: false, message: msg };
  }
};
