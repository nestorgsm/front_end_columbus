// export const myMutation = ( state ) => {

// }

export const setADifferentProduct = (state, product) => {
  state.differentProducts.push(product);
  state.totalDifferentProducts += 1;
};

export const setNumOfEachProduct = (state, product) => {
  state.numProducts.push(product);
};

export const addProduct = (state, product) => {
  state.numProducts[
    state.numProducts.findIndex((elem) => elem.id === product)
  ].cantidad += 1;
};

export const subProduct = (state, product) => {
  state.numProducts[
    state.numProducts.findIndex((elem) => elem.id === product)
  ].cantidad -= 1;
};

export const onLogOut = (state) => {
  state.differentProducts = [];
  state.totalDifferentProducts = 0;
  state.numProducts = [];
};
