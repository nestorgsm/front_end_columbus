# front-end-columbus

Front-end para prueba técnica

### Requisitos 📋

- Vue 3

---

### Instalación 🔌

```bash
git clone https://gitlab.com/nestorgsmf/front_end_columbus.git
cd front_end_columbus
npm install
```

---

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Construido con 🛠️

- Visual Studio Code
- Composition API
- Vuex
- Vue Router
- Axios
- PrimeVUE

---

### Autor 🎎️

- Néstor Sánchez
